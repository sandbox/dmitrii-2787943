<?php

/**
 * @file
 * Class definition of FeedsNodeFudeProcessor.
 */

// Update mode for non-existent items.
define('FUDE_FEEDS_SKIP_NON_EXISTENT', 103);
define('FUDE_FEEDS_UNPUBLISH_NON_EXISTENT', 104);
define('FUDE_FEEDS_REMOVE_NON_EXISTENT', 105);

/**
 * Creates nodes from feed items.
 *
 * Node processor to unpublish or delete nodes not included in the feed.
 */
class FeedsNodeFudeProcessor extends FeedsNodeProcessor {

  /** @var array of processed nids. */
  protected $processed_nids = array();

  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {

    // Keep track of processed items in this pass, set total number of items.
    $processed = 0;
    $batch_size = variable_get('feeds_node_batch_size', FEEDS_NODE_BATCH_SIZE);
    if (!$batch->getTotal(FEEDS_PROCESSING)) {
      $batch->setTotal(FEEDS_PROCESSING, count($batch->items));
    }

    while ($item = $batch->shiftItem()) {

      // Create/update if item does not exist or update existing is enabled.
      $nid = $this->existingItemId($batch, $source);
      // Add the nid to the processed nids list.
      if ($nid != 0) {
        $this->processed_nids[$nid] = $nid;
      }
      if (!($nid = $this->existingItemId($batch, $source)) || ($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {
        // Only proceed if item has actually changed.
        $hash = $this->hash($item);
        if (!empty($nid) && $hash == $this->getHash($nid)) {
          // Process unpublished nodes.
          $node = db_fetch_object(db_query("SELECT created, nid, vid, status FROM {node} WHERE nid = %d", $nid));
          if (!empty($node->status)) {
            continue;
          }
        }

        $node = $this->buildNode($nid, $source->feed_nid);
        $node->feeds_node_item->hash = $hash;

        // Map and save node. If errors occur don't stop but report them.
        try {
          $this->map($batch, $node);
          if ($this->config['authorize']) {
            if (empty($node->nid)) {
              $op = 'create';
            }
            else {
              $op = 'update';
            }
            $account = user_load($node->uid);
            if (!node_access($op, $node, $account)) {
              throw new Exception('User ' . $account->uid . ' not authorized to ' . $op . ' content type ' . $node->type);
            }
          }
          // Set node status to published.
          $node->status = 1;
          node_save($node);
          if (!empty($nid)) {
            $batch->updated++;
          }
          else {
            $batch->created++;
          }
        }
        catch (Exception $e) {
          drupal_set_message($e->getMessage(), 'warning');
          watchdog('feeds', $e->getMessage(), array(), WATCHDOG_WARNING);
        }
      }

      $processed++;
      if ($processed >= $batch_size) {
        $total = $batch->getTotal(FEEDS_PROCESSING);
        $batch->setProgress(FEEDS_PROCESSING, $total - count($batch->items));
        return;
      }
    }

    // Set messages.
    if ($batch->created) {
      drupal_set_message(format_plural($batch->created, 'Created @number @type node.', 'Created @number @type nodes.', array('@number' => $batch->created, '@type' => node_get_types('name', $this->config['content_type']))));
    }
    if ($batch->updated) {
      drupal_set_message(format_plural($batch->updated, 'Updated @number @type node.', 'Updated @number @type nodes.', array('@number' => $batch->updated, '@type' => node_get_types('name', $this->config['content_type']))));
    }
    if (!$batch->created && !$batch->updated) {
      drupal_set_message(t('There is no new content.'));
    }
    $batch->setProgress(FEEDS_PROCESSING, FEEDS_BATCH_COMPLETE);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $config = parent::configDefaults();

    $config['update_non_existent'] = FUDE_FEEDS_SKIP_NON_EXISTENT;

    return $config;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    $form['update_non_existent'] = array(
      '#type' => 'radios',
      '#title' => t('Update nodes missing in the feed'),
      '#description' => t('Select how nodes missing in the feed should be updated.'),
      '#options' => array(
        FUDE_FEEDS_SKIP_NON_EXISTENT => 'Skip non existent nodes',
        FUDE_FEEDS_UNPUBLISH_NON_EXISTENT => 'Unpublish non existent nodes',
        FUDE_FEEDS_REMOVE_NON_EXISTENT => 'Remove non existent nodes',
      ),
      '#default_value' => $this->config['update_non_existent'],
    );

    return $form;
  }

  /**
   * Returns a list of nid's that originated from the FeedSource.
   *
   * @param $source FeedsSource
   *   Feed source object.
   *
   * @return array
   *   Array of Node ids.
   */
  public function getFeedNids(FeedsSource $source) {
    $nids = array();

    // Pull out nids
    // From a node based feed
    if ($source->feed_nid > 0) {
      $results = db_query("SELECT nid FROM {feeds_node_item} WHERE feed_nid = %d", $source->feed_nid);
    }
    // From an import form feed
    else {
      $results = db_query("SELECT nid FROM {feeds_node_item} WHERE id = '%s'", $source->id);
    }
    while ($result = db_fetch_array($results)) {
      $nids[$result['nid']] = $result['nid'];
    }

    return $nids;
  }

}
